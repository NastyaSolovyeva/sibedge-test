// @flow
import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {UsersLayout} from 'modules/users/components/UserLayout';
import {NoMatchRouter} from './NoMatchRouter';

export const AppRouter = () => (
    <BrowserRouter>
        <Switch>
            <Route component={UsersLayout} exact path="/" />
            <Route component={NoMatchRouter} />
        </Switch>
    </BrowserRouter>
);
