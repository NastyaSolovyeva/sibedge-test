// @flow
import {rootReducer} from 'app/reducers';
import {UserApi} from 'modules/users/services/api';
import thunk from 'redux-thunk';
import {makeStore} from './makeStore';

export function createAppStore() {
    const extraArgs = {
        userApi: new UserApi(),
    };

    const store = makeStore({
        debug: 'production' !== process.env.NODE_ENV,
        middlewares: [thunk.withExtraArgument(extraArgs)],
        reducer: rootReducer,
    });

    return store;
}
