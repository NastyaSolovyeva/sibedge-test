// @flow
import {combineReducers} from 'redux';
import {userReducers} from 'modules/users/reducers';
import {USER_MODULE_NAME} from 'modules/users/constants';

export const rootReducer = combineReducers({
    [USER_MODULE_NAME]: userReducers,
});
