// @flow
import {USER_ACTION_TYPE} from 'modules/users/constants/';

export type TUserItemId = number;

const SubscrType = {
    ARTICLE: 'Article',
    NEWS: 'News',
    VIDEO: 'Video',
};

export type TUserItem = {
    date: string,
    email: string,
    id: TUserItemId,
    name: string,
    subscription: typeof SubscrType.ARTICLE | typeof SubscrType.NEWS | typeof SubscrType.VIDEO,
};

export type TUserData = Array<TUserItem>;

export type TUserState = $ReadOnly<{|
    data: TUserData,
    isLoading: boolean,
|}>;

type TUser = TUserData | TUserItem | TUserItemId;

export type TActionGetUserStart = {type: USER_ACTION_TYPE.GET_USER_START};
export type TActionGetUserFail = {type: USER_ACTION_TYPE.GET_USER_FAIL};
export type TActionGetUserSuccess = {payload: TUser, type: USER_ACTION_TYPE.GET_USER_SUCCESS};

export type TActionCreateUserStart = {type: USER_ACTION_TYPE.CREATE_USER_START};
export type TActionCreateUserFail = {type: USER_ACTION_TYPE.CREATE_USER_FAIL};
export type TActionCreateUserSuccess = {payload: TUser, type: USER_ACTION_TYPE.CREATE_USER_SUCCESS};

export type TActionDeleteUserStart = {type: USER_ACTION_TYPE.DELETE_USER_START};
export type TActionDeleteUserFail = {type: USER_ACTION_TYPE.DELETE_USER_FAIL};
export type TActionDeleteUserSuccess = {payload: TUser, type: USER_ACTION_TYPE.DELETE_USER_SUCCESS};

export type TUserAction =
    | TActionGetUserStart
    | TActionGetUserFail
    | TActionGetUserSuccess
    | TActionCreateUserSuccess
    | TActionCreateUserFail
    | TActionCreateUserStart
    | TActionDeleteUserSuccess
    | TActionDeleteUserFail
    | TActionDeleteUserStart;
