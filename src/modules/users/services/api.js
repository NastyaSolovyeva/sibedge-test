// @flow
import type {TUserItem, TUserItemId} from 'modules/users/type';

export class UserApi {
    async getUser() {
        return await (await fetch('http://localhost:3000/users')).json();
    }

    async createUser(data: TUserItem): Promise<TUserItem> {
        return await (await fetch('http://localhost:3000/users', {
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
            method: 'post',
        })).json();
    }

    async deleteUser(id: TUserItemId) {
        return await (await fetch(`http://localhost:3000/users/${id}`, {
            headers: {
                'Content-Type': 'application/json',
            },
            method: 'delete',
        })).json();
    }
}
