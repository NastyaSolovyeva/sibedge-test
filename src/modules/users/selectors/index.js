// @flow
import {USER_MODULE_NAME} from 'modules/users/constants';
import type {TRootState} from 'modules/types';
import {createSelector} from 'reselect';

export function selectModule(state: TRootState) {
    return state[USER_MODULE_NAME];
}

export function selectUserState(state: TRootState) {
    return selectModule(state).users;
}

export function selectUserData(state: TRootState) {
    return selectUserState(state).data;
}

export function selectUserIsLoading(state: TRootState) {
    return selectUserState(state).isLoading;
}

const selectId = (state, {id}) => id;

export const selectUserById = createSelector(
    [selectUserData, selectId],
    (data, id) => data.find((item) => item.id === id)
);
