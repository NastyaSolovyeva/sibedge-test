// @flow
import React from 'react';
import {Formik, Field, ErrorMessage} from 'formik';
import connect from 'react-redux/es/connect/connect';
import {compose} from 'redux';
import * as Yup from 'yup';
import bemCn from 'bem-cn';
import {createUser} from 'modules/users/actions';
import styles from './styles.less';

const b = bemCn('subscribe-form');

type TProps = {
    onSubmit: typeof createUser,
};

export class AddFormWrapper extends React.Component<TProps> {
    getCurrentDatetime = () => {
        return new Date().toLocaleString();
    };

    render() {
        const {onSubmit: handleSubmitGlobal} = this.props;

        const initialValue = {
            date: this.getCurrentDatetime(),
            email: '',
            name: '',
            subscription: 'news',
        };

        const validationSchema = Yup.object().shape({
            email: Yup.string()
                .email('Compliance with xxx@yyy.zzz format required')
                .matches(/^[a-zA-Z@._]*$/, 'Email can only contain Latin letters.')
                .required('Email is required'),
            name: Yup.string()
                .max(60, 'Too long name. Max length = 60 letters')
                .matches(/^[a-zA-Z ]*$/, 'Name can only contain Latin letters.')
                .required('Name is required'),
        });

        return (
            <Formik
                enableReinitialize
                initialValues={initialValue}
                onReset={initialValue}
                onSubmit={handleSubmitGlobal}
                validationSchema={validationSchema}
            >
                {({values, errors, touched, handleChange, handleSubmit, isSubmitting, resetForm, isValid}) => (
                    <form className={b()} onSubmit={handleSubmit}>
                        <div className={b('title')}>subscription</div>
                        <div className={b('form-group')}>
                            <label className={b('label')} htmlFor="name">
                                Name
                            </label>
                            <Field
                                className={b('input') + (errors.name && touched.name ? ' is-invalid' : '')}
                                name="name"
                                onChange={handleChange}
                                placeholder={'Name'}
                                type="text"
                                value={values.name || ''}
                            />
                            <ErrorMessage className={b('invalid-feedback')} component="div" name="name" />
                        </div>

                        <div className={b('form-group')}>
                            <label className={b('label')} htmlFor="email">
                                Email
                            </label>
                            <Field
                                className={b('input') + (errors.email && touched.email ? ' is-invalid' : '')}
                                name="email"
                                onChange={handleChange}
                                placeholder={'Email'}
                                type="text"
                                value={values.email || ''}
                            />
                            <ErrorMessage className={b('invalid-feedback')} component="div" name="email" />
                        </div>

                        <div className={b('form-group')}>
                            <label className={b('label')} htmlFor="email">
                                Subscription type
                            </label>
                            <Field className={b('input')} component="select" name="subscription" onChange={handleChange}>
                                <option selected value="news">
                                    News
                                </option>
                                <option value="article">Article</option>
                                <option value="video">Video</option>
                            </Field>
                        </div>

                        <Field name="date" type="hidden" value={this.getCurrentDatetime()} />

                        <div className={b('btn-group')}>
                            <button className={'btn btn-reset'} onClick={resetForm} type="reset">
                                Reset
                            </button>

                            <button className={'btn btn-submit'} disabled={!isValid && isSubmitting} type="submit">
                                Add User
                            </button>
                        </div>
                    </form>
                )}
            </Formik>
        );
    }
}

export const AddForm = compose(
    connect(
        null,
        {createUser}
    )
)(AddFormWrapper);
