// @flow
import React from 'react';
import bemCn from "bem-cn";
import type {TUserItemId} from 'modules/users/type';
import styles from './styles.less';

const b = bemCn('action');

type TProps = {
    id: TUserItemId,
    onDelete: (id: TUserItemId) => Promise<any>,
};

export const ActionDelete = ({id, onDelete}: TProps) => {
    function handleDelete() {
        onDelete(id);
    }

    return (
        <button className={b('btn-delete')} onClick={handleDelete} type="submit">
            Delete
        </button>
    );
};
