// @flow
import React from 'react';
import bemCn from 'bem-cn';
import type {TUserItem, TUserItemId} from 'modules/users/type';
import {ActionDelete} from 'modules/users/components/ActionDelete';
import styles from './styles.less';

const b = bemCn('table');

type TProps = {
    data: Array<TUserItem>,
    onDelete: (id: TUserItemId) => Promise<any>,
};

export class UserTable extends React.Component<TProps> {
    render() {
        const userList = this.props.data;
        return (
            <div className={b()}>
                <div className={b('header')}>
                    <div className={b('item')}>ID</div>
                    <div className={b('item')}>Name</div>
                    <div className={b('item')}>Email</div>
                    <div className={b('item')}>Date</div>
                    <div className={b('item')}>Subscription</div>
                    <div className={b('item')}>More</div>
                </div>
                {userList.map(({id, date, name, email, subscription}) => (
                    <div className={b('row')} key={id}>
                        <div className={b('item')}>{id}</div>
                        <div className={b('item')}>{name}</div>
                        <div className={b('item')}>{email}</div>
                        <div className={b('item')}>{date}</div>
                        <div className={b('item')}>{subscription}</div>
                        <div className={b('item')}>
                            <ActionDelete id={id} onDelete={this.props.onDelete} />
                        </div>
                    </div>
                ))}
            </div>
        );
    }
}
