// @flow
import React from 'react';
import connect from 'react-redux/es/connect/connect';
import {compose} from 'redux';
import bemCn from 'bem-cn';
import {createUser, deleteUser, getUser as getUserList} from 'modules/users/actions';
import type {TUserData, TUserItem, TUserItemId} from 'modules/users/type/';
import {selectUserData, selectUserIsLoading} from 'modules/users/selectors';
import {UserTable} from 'modules/users/components/UserTable';
import {AddForm} from 'modules/users/components/AddForm';
import styles from './styles.less';

const b = bemCn('user-page');

type TProps = {
    createUser: typeof createUser,
    deleteUser: typeof deleteUser,
    getUserList: typeof getUserList,
    userList: TUserData,
    userListIsLoading: boolean,
};

export class UserWrapper extends React.Component<TProps> {
    componentDidMount() {
        const {getUserList, userList, userListIsLoading} = this.props;

        if (!userList.length && !userListIsLoading) {
            getUserList();
        }
    }

    handleDelete = async (id: TUserItemId) => {
        await this.props.deleteUser(id);
    };

    handleSubmit = async (values: TUserItem) => {
        const {userList} = this.props;

        const match = userList.find((user) => {
            return user.email === values.email;
        });

        // Search for duplicate users
        if (match) {
            if (match.name === values.name) {
                alert(`This user: ${match.name} already exists!`);
            } else {
                alert(`A user with this email: ${match.email} already exists!`);
            }
        } else {
            await this.props.createUser(values);
        }
    };

    render() {
        return (
            <div className={b()}>
                <div className={b('form')}>
                    <AddForm onSubmit={this.handleSubmit} />
                </div>
                <div className={b('user-table')}>
                    <UserTable data={this.props.userList} onDelete={this.handleDelete} />
                </div>
            </div>
        );
    }
}

export const UsersLayout = compose(
    connect(
        (state) => {
            return {
                userList: selectUserData(state),
                userListIsLoading: selectUserIsLoading(state),
            };
        },
        {
            createUser,
            deleteUser,
            getUserList,
        }
    )
)(UserWrapper);
