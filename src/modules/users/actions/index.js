// @flow
import {USER_ACTION_TYPE} from 'modules/users/constants';
import type {TThunkAction} from 'modules/types';
import type {TUserItem, TUserItemId} from 'modules/users/type';

// ------------ GET --------------
function getUserStart() {
    return {
        type: USER_ACTION_TYPE.GET_USER_START,
    };
}

function getUserSuccess(data) {
    return {
        payload: data,
        type: USER_ACTION_TYPE.GET_USER_SUCCESS,
    };
}

export function getUser(): TThunkAction {
    return async (dispatch, getState, {userApi}) => {
        dispatch(getUserStart());
        const data = await userApi.getUser();
        dispatch(getUserSuccess(data));
    };
}

// ------------ CREATE --------------
function createUserStart() {
    return {
        type: USER_ACTION_TYPE.CREATE_USER_START,
    };
}

function createUserSuccess(newUser) {
    return {
        payload: newUser,
        type: USER_ACTION_TYPE.CREATE_USER_SUCCESS,
    };
}

export function createUser(user: TUserItem): TThunkAction {
    return async (dispatch, getState, {userApi}) => {
        dispatch(createUserStart());
        const newUser = await userApi.createUser(user);
        dispatch(createUserSuccess(newUser));
    };
}

// ------------ DELETE --------------
function deleteUserStart() {
    return {
        type: USER_ACTION_TYPE.DELETE_USER_START,
    };
}

function deleteUserSuccess(id) {
    return {
        payload: id,
        type: USER_ACTION_TYPE.DELETE_USER_SUCCESS,
    };
}

export function deleteUser(id: TUserItemId): TThunkAction {
    return async (dispatch, getState, {userApi}) => {
        dispatch(deleteUserStart());
        await userApi.deleteUser(id);
        dispatch(deleteUserSuccess(id));
    };
}
