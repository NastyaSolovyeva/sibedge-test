// @flow
import {combineReducers} from 'redux';
import {userReducer} from 'modules/users/reducers/users';

export const userReducers = combineReducers({
    users: userReducer,
});
