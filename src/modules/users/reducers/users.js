// @flow
import {USER_ACTION_TYPE} from 'modules/users/constants/';
import type {TUserAction, TUserState} from 'modules/users/type';

export const initialState: TUserState = {
    data: [],
    isLoading: false,
};

const reducer = {
    // ------------------- GET -------------------
    [USER_ACTION_TYPE.GET_USER_FAIL](state) {
        return {
            ...state,
            isLoading: false,
        };
    },
    [USER_ACTION_TYPE.GET_USER_START](state) {
        return {
            ...state,
            isLoading: true,
        };
    },
    [USER_ACTION_TYPE.GET_USER_SUCCESS](state, {payload}) {
        return {
            ...state,
            data: payload,
            isLoading: false,
        };
    },
    // ------------------- CREATE -------------------
    [USER_ACTION_TYPE.CREATE_USER_FAIL](state) {
        return {
            ...state,
            isLoading: false,
        };
    },
    [USER_ACTION_TYPE.CREATE_USER_START](state) {
        return {
            ...state,
            isLoading: true,
        };
    },
    [USER_ACTION_TYPE.CREATE_USER_SUCCESS](state, {payload}) {
        return {
            ...state,
            data: [...state.data, payload],
            isLoading: false,
        };
    },
    // ------------------- DELETE -------------------
    [USER_ACTION_TYPE.DELETE_USER_FAIL](state) {
        return {
            ...state,
            isLoading: false,
        };
    },
    [USER_ACTION_TYPE.DELETE_USER_START](state) {
        return {
            ...state,
            isLoading: true,
        };
    },
    [USER_ACTION_TYPE.DELETE_USER_SUCCESS](state, {payload}) {
        return {
            ...state,
            data: state.data.filter((item) => item.id !== payload),
            isLoading: false,
        };
    },
};

export function userReducer(state: TUserState = initialState, {payload, type}: TUserAction = {}) {
    if (reducer) {
        return type in reducer ? reducer[type](state, {payload, type}) : state;
    }

    // switch (type) {
    //     case USER_ACTION_TYPE.GET_USER_FAIL: {
    //         return {
    //             ...state,
    //             isLoading: false,
    //         };
    //     }
    //     case USER_ACTION_TYPE.GET_USER_START: {
    //         return {
    //             ...state,
    //             isLoading: true,
    //         };
    //     }
    //     case USER_ACTION_TYPE.GET_USER_SUCCESS: {
    //         return {
    //             ...state,
    //             data: payload,
    //             isLoading: false,
    //         };
    //     }
    //         // ------------------- CREATE -------------------
    //     case USER_ACTION_TYPE.CREATE_USER_FAIL: {
    //         return {
    //             ...state,
    //             isLoading: false,
    //         };
    //     }
    //     case USER_ACTION_TYPE.CREATE_USER_START:  {
    //         return {
    //             ...state,
    //             isLoading: true,
    //         };
    //     }
    //     case USER_ACTION_TYPE.CREATE_USER_SUCCESS: {
    //         return {
    //             ...state,
    //             data: [...state.data, payload],
    //             isLoading: false,
    //         };
    //     }
    //         // ------------------- DELETE -------------------
    //     case USER_ACTION_TYPE.DELETE_USER_FAIL: {
    //         return {
    //             ...state,
    //             isLoading: false,
    //         };
    //     }
    //     case USER_ACTION_TYPE.DELETE_USER_START: {
    //         return {
    //             ...state,
    //             isLoading: true,
    //         };
    //     }
    //     case USER_ACTION_TYPE.DELETE_USER_SUCCESS: {
    //         return {
    //             ...state,
    //             data: state.data.filter((item) => item.id !== payload),
    //             isLoading: false,
    //         };
    //     }
    // }

}
