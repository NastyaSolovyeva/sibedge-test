// @flow
import type {TUserState} from 'modules/users/type/';
import type {UserApi} from 'modules/users/services/api';

// from extra args
type TExtraArgs = {
    userApi: UserApi,
};

type TPromiseAction = Promise<Object>;
type TGetState = () => any;
type TDispatch = (action: Object | TThunkAction | TPromiseAction | Array<Object>) => any;
export type TThunkAction = (dispatch: TDispatch, getState: TGetState, extraArgs: TExtraArgs) => any;

// from root reducer
export type TRootState = {
    user: {
        user: TUserState,
    },
};
