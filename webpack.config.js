const path = require('path');
const webpack = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const publicPath = path.resolve(__dirname, 'www');
const buildDir = path.resolve(publicPath, 'build');
const sourcePath = path.resolve(__dirname, 'src');

const styleLoader = MiniCssExtractPlugin.loader;
const cssLoader = {
    loader: 'css-loader',
    options: {modules: false, sourceMap: true},
};
const cssModulesLoader = {
    loader: 'css-loader',
    options: {modules: true, sourceMap: true},
};
const lessLoader = {
    loader: 'less-loader',
    options: {javascriptEnabled: true, sourceMap: true},
};

const cssPlugin = new MiniCssExtractPlugin({
    chunkFilename: `${buildDir}/css/[id].min.css`,
    filename: `${buildDir}/css/[name].min.css`,
});

const htmlPlugin = new HtmlWebPackPlugin({
    filename: 'index.html',
    template: path.resolve(sourcePath, 'index.html'),
});


module.exports = {
    context: sourcePath,
    devServer: {
        port: 8080,
        contentBase: publicPath,
        before(app) {
            app.get('/api/v1/config', (req, res) => {
                res.json({
                    debug: 1,
                    host: 'http://rest-api-host.local',
                });
            });
        },
    },
    entry: ['@babel/polyfill', 'whatwg-fetch', './index'],
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                },
            },
            {
                test: /\.(less|css)$/u,
                use: ['style-loader', 'css-loader', 'postcss-loader', lessLoader],
            },
        ],
    },
    optimization: {
        minimizer: [
            new OptimizeCSSAssetsPlugin({
                cssProcessorOptions: {
                    autoprefixer: false,
                    discardUnused: false,
                    map: {
                        inline: false,
                    },
                    mergeIdents: false,
                    reduceIdents: false,
                    safe: true,
                    zIndex: false,
                },
            }),
            new UglifyJsPlugin({
                cache: true,
                parallel: true,
                sourceMap: true,
            }),
        ],
    },
    plugins: [cssPlugin, htmlPlugin],
    output: {
        filename: `[name].min.js`,
        path: buildDir,
        publicPath: '/',
    },
    resolve: {
        extensions: ['*', '.js', '.jsx'],
        modules: [sourcePath, 'node_modules'],
    },
};
